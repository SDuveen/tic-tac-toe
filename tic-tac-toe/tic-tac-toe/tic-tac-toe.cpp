#include <iostream>
#include <ctime>
#include <vector>
#include "ai.h"

char board[3][3] = { {'1', '2', '3'}, {'4', '5', '6'}, {'7', '8', '9'} };
int player;
char marker;
int turn = 1;

void drawBoard() {
	
	std::cout << "\n";
	std::cout << " " << board[0][0] << " | " << board[0][1] << " | " << board[0][2] << "\n";
	std::cout << "-----------\n";
	std::cout << " " << board[1][0] << " | " << board[1][1] << " | " << board[1][2] << "\n";
	std::cout << "-----------\n";
	std::cout << " " << board[2][0] << " | " << board[2][1] << " | " << board[2][2] << "\n";
	std::cout << "\n";
}

void switchPlayer() {

	if (player == 1) {
		player = 2;
		marker = 'O';
	}
	else {
		player = 1;
		marker = 'X';
	}
}

void placeMarker(int spot) {
	int row = spot / 3;
	int col;

	if (spot % 3 == 0) {
		row = row - 1;
		col = 2;
	}
	else {
		col = (spot % 3) - 1;
	}

	if (board[row][col] == ' ') {
		board[row][col] = marker;
		turn++;
	} 
	else if (spot > 0 && spot <= 9) {
		std::cout << "\nThis slot already has a marker, try a different one. \n\n";
		switchPlayer();
	}
	else {
		std::cout << "\nThis slotnumber is invalid, try a different one. \n\n";
		switchPlayer();
	}
}

void clearBoard() {
	
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			board[i][j] = ' ';
		}
	}
}

bool checkWinner(char board[3][3]) {
	if (board[0][0] == marker && board[0][1] == marker && board[0][2] == marker) {
		return true;
	}
	else if (board[1][0] == marker && board[1][1] == marker && board[1][2] == marker) {
		return true;
	}
	else if (board[2][0] == marker && board[2][1] == marker && board[2][2] == marker) {
		return true;
	}
	else if (board[0][0] == marker && board[1][0] == marker && board[2][0] == marker) {
		return true;
	}
	else if (board[0][1] == marker && board[1][1] == marker && board[2][1] == marker) {
		return true;
	}
	else if (board[0][2] == marker && board[1][2] == marker && board[2][2] == marker) {
		return true;
	}
	else if (board[0][0] == marker && board[1][1] == marker && board[2][2] == marker) {
		return true;
	}
	else if (board[2][0] == marker && board[1][1] == marker && board[0][2] == marker) {
		return true;
	}
	else {
		return false;
	}
}

void playerInit() {
	srand(time(NULL));
	player = rand() % 2 + 1;

	if (player == 1) {
		marker = 'X';
	}
	else if (player == 2) {
		marker = 'O';
	}
}

int main()
{
	int players;
	std::cout << "Enter the amount of players (1/2): ";
	std::cin >> players;

	if (players == 1) {
		drawBoard();
		clearBoard();
		playerInit();

		while (turn <= 9) {

			drawBoard();

			std::cout << "Player " << player << "'s turn!\n";

			int spot;

			if (player == 1) {
				std::cout << "Enter where you want to place your marker: ";
				std::cin >> spot;
			}
			else {
				spot = AI::miniMax(board, 'O').getIndex();
			}

			placeMarker(spot);

			if (checkWinner(board)) {
				break;
			}

			switchPlayer();
		}

		if (checkWinner(board)) {
			drawBoard();
			std::cout << "\nCongratulations player " << player << " you won!\n";
		}
		else {
			drawBoard();
			std::cout << "\nIt's a tie...\n";
		}
	}
	else if (players == 2) {
		drawBoard();
		clearBoard();
		playerInit();

		while (turn <= 9) {

			drawBoard();

			std::cout << "Player " << player << "'s turn!\n";
			std::cout << "Enter where you want to place your marker: ";

			int spot;
			std::cin >> spot;
			placeMarker(spot);

			if (checkWinner(board)) {
				break;
			}

			switchPlayer();
		}

		if (checkWinner(board)) {
			drawBoard();
			std::cout << "\nCongratulations player " << player << " you won!\n";
		}
		else {
			drawBoard();
			std::cout << "\nIt's a tie...\n";
		}
	}
	else {
		std::cout << "Sorry, this game can only be played with 1 or 2 players.";
	}
}
