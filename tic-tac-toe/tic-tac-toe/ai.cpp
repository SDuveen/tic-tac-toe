#include "ai.h"
#include <iostream>
#include <vector>

std::vector<int> AI::getEmpty(char board[3][3]) {
	
	std::vector<int> indices;
	int spot = 1;

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			
			if (board[i][j] == ' ') {
				indices.push_back(spot);
			}

			spot++;
		}
	}

	return indices;
}

bool AI::checkWin(char board[3][3], char marker) {
	if (board[0][0] == marker && board[0][1] == marker && board[0][2] == marker) {
		return true;
	}
	else if (board[1][0] == marker && board[1][1] == marker && board[1][2] == marker) {
		return true;
	}
	else if (board[2][0] == marker && board[2][1] == marker && board[2][2] == marker) {
		return true;
	}
	else if (board[0][0] == marker && board[1][0] == marker && board[2][0] == marker) {
		return true;
	}
	else if (board[0][1] == marker && board[1][1] == marker && board[2][1] == marker) {
		return true;
	}
	else if (board[0][2] == marker && board[1][2] == marker && board[2][2] == marker) {
		return true;
	}
	else if (board[0][0] == marker && board[1][1] == marker && board[2][2] == marker) {
		return true;
	}
	else if (board[2][0] == marker && board[1][1] == marker && board[0][2] == marker) {
		return true;
	}
	else {
		return false;
	}
}

Move::Move(int new_index, int new_score) {
	index = new_index;
	score = new_score;
}

int Move::getIndex() {
	return index;
}

int Move::getScore() {
	return score;
}

void Move::setIndex(int new_index) {
	index = new_index;
}

void Move::setScore(int new_score) {
	score = new_score;
}

Move AI::miniMax(char board[3][3], char marker) {

	std::vector<int> available = AI::getEmpty(board);

	if (checkWin(board, 'X')) {
		return Move(-1, 10);
	}	
	else if (checkWin(board, 'O')) {
		return Move(-1, -10);
	}
	else if (available.size() == 0) {
		return Move(-1, 0);
	}

	std::vector<Move> moves;

	for (int i = 0; i < available.size(); i++) {
		int index = available[i];

		board[(index - 1) / 3][(index - 1) % 3] = marker;
		int score;
		
		if (marker == 'X') {
			score = miniMax(board, 'O').getScore();
		} 
		else {
			score = miniMax(board, 'X').getScore();
		}

		board[(index - 1) / 3][(index - 1) % 3] = ' ';
		Move move(index, score);

		moves.push_back(move);
	}

	Move bestMove(-1, -1);

	if (marker == 'O') {
		int lowest = 10000;

		for (int i = 0; i < moves.size(); i++) {
			
			if (moves[i].getScore() < lowest) {
				lowest = moves[i].getScore();
				bestMove = Move(moves[i].getIndex(), lowest);
			}
		}
	}
	else {
		int highest = -10000;

		for (int i = 0; i < moves.size(); i++) {

			if (moves[i].getScore() > highest) {
				highest = moves[i].getScore();
				bestMove = Move(moves[i].getIndex(), highest);
			}
		}
	}

	return bestMove;
}