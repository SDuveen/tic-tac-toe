#include <vector>

class Move {
	int index;
	int score;

public:
	Move(int index, int score);
	int getIndex();
	int getScore();
	void setIndex(int index);
	void setScore(int score);
};

class AI {
	static bool checkWin(char board[3][3], char marker);
	static std::vector<int> getEmpty(char board[3][3]);

public:
	static Move miniMax(char board[3][3], char marker);
};